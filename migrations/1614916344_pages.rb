Sequel.migration do
  change do
    create_table :pages do
      uuid :id, primary_key: true, default: Sequel.function(:uuid_generate_v4)

      String :slug, null: false
      DateTime :created, null: false, default: Sequel.function(:NOW)
    end

    create_table :page_revisions do
      uuid :id, primary_key: true, default: Sequel.function(:uuid_generate_v4)
      foreign_key :page_id, :pages, type: :uuid, null: true, index: true

      String :content

      foreign_key :creator, :users, null: true
      DateTime :created, null: false, default: Sequel.function(:NOW)
    end

    alter_table :pages do
      add_foreign_key :current_revision_id, :page_revisions, type: :uuid, null: false, index: true
    end
  end
end
