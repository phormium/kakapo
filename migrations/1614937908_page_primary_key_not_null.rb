Sequel.migration do
  up do
    alter_table :pages do
      set_column_not_null :id
    end

    alter_table :page_revisions do
      set_column_not_null :id
    end
  end

  down do
    # nothing!
  end
end
