Sequel.migration do
  change do
    alter_table :page_revisions do
      add_column :description, String, null: true
    end
  end
end
