require 'securerandom'

module Kakapo
  APP_CONFIG_ENTRIES = {
    "site-name" => {
      :type => :text,
      :default => "Kākāpō",
    },
    "org-name" => {
      :type => :text,
      :default => "Example Organisation",
    },
    "base-url" => {
      :type => :text,
      :default => "https://localhost",
    },
    "display-version" => {
      :type => :bool,
      :default => false,
    },
    "file-storage-dir" => {
      :type => :text,
      :default => '@SITEDIR@/files/',
    },
    "magenta-providers" => {
      :type => :json,
      :default => '[]',
    },
    "anonymous-view" => {
      :type => :bool,
      :default => true,
    },
    "anonymous-edit" => {
      :type => :bool,
      :default => false,
    },
    "anonymous-history" => {
      :type => :bool,
      :default => true,
    },
  }

  APP_CONFIG_DEPRECATED_ENTRIES = {
  }

  class Application
    configure do
      set :session_secret, ENV.fetch('SESSION_SECRET') {SecureRandom.hex(32)}
    end
  end
end
