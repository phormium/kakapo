:'meta_description': "English"

# Languages
:'language/change': "Change language"
:'language/invalid': "The chosen language was not found."

# Generic actions
:'back': "Back"
:'back/home': "Go back to the home page"
:'change': "Change"
:'submit': "Submit"
:'edit': "Edit"
:'save_changes': "Save changes"
:'search': "Search"
:'search_term': "Search term"
:'new_search': "New search"
:'download': "Download"
:'upload': "Upload"
:'view': "View"

# Fields
:'user_id': "User ID"
:'user_name': "Username"
:'email_address': "Email address"
:'bool/yes': "yes"
:'bool/no': "no"
:'enabled/yes': "enabled"
:'enabled/no': "disabled"
:'thats_you': "That's you!"
:'unknown': "(unknown)"
:'unconfirmed': "(unconfirmed)"
:'required': "Required"
:'author': "Author"
:'timestamp/fmt_ts': "<%= ts.strftime('%Y-%m-%d %H:%M:%S %z') %>"
:'timestamp/created': "Created"
:'timestamp/edited': "Edited"

# Some generic stuff
:'required_field_missing': "A required field was not provided."
:'invalid_action': "The provided action was invalid."
:'must_log_in': "You must log in to perform this action."

# Sidebar miscellaneous
:'sidebar/user/hello': "Hi, <%= e(username) %>!"
:'sidebar/user/settings': "Settings"
:'sidebar/user/logout': "Log out"
:'sidebar/user/not_logged_in': "Not logged in."
:'sidebar/user/ip_user': "IP: <code><%= e(ip) %></code>"
:'sidebar/user/login': "Log in"

# Authentication - log in
:'auth/choose_provider/title': "Log in"
:'auth/choose_provider/content': "To log in to <%= e(site_name) %>, choose an identity provider from the list below."
:'auth/generic_error/title': "Authentication error"
:'auth/generic_error/content': "An error occurred while trying to log you in. Please contact the <%= e(site_name) %> administrators."
:'auth/success': "You have successfully logged in via <%= e(provider) %>. Welcome, <%= e(username) %>!"
:'auth/new_user_welcome': "Welcome to <%= e(site_name) %>! You have been assigned a random username, which you can change at any time from your settings page (accessible from the sidebar)."
:'auth/logout/success': "You have successfully logged out."

# User settings
:'user/settings/title': "User settings"
:'user/settings/username/section_title': "Change username"
:'user/settings/username/current_username': "Your current username"
:'user/settings/username/new_username': "New username"
:'user/settings/username/submit': "Change username"
:'user/settings/username/errors/special_characters': "Your chosen username contains invalid characters. Please pick another username. Usernames can only contain letters and numbers, with no punctuation or special characters."
:'user/settings/username/errors/already_taken': "Someone is already using that username. Please pick another."
:'user/settings/username/success': "Successfully changed your username to <code><%= e(username) %></code>!"

# Page - actions
:'page/actions/view': "Read"
:'page/actions/edit': "Edit page"
:'page/actions/create': "Create page"
:'page/actions/history': "History"
:'page/actions/admin': "Admin actions"
:'page/actions/admin/delete': "Delete page"
:'page/actions/admin/rename': "Rename page"

# Page - not found
:'page/not_found/content': "The page <code><%= e(slug) %></code> does not exist. You can create it by clicking the \"<%= t(:'page/actions/create') %>\" button in the top right of the page."

# Page - edit
:'page/edit/content': "Page content"
:'page/edit/revdesc': "Brief description of this edit"
:'page/edit/submit': "Preview this edit"
:'page/edit/preview/actions': "Edit actions"
:'page/edit/preview/return': "Return to the editor"
:'page/edit/preview/submit': "Save changes"
:'page/edit/success': "Successfully saved this edit!"
:'page/edit/preview/warnings/not_saved': "This is a preview of your edit. To save your changes, click the \"<%= t(:'page/edit/preview/submit') %>\" button at the bottom of this page."
:'page/edit/warnings/unauthenticated': "You are not currently logged in to <%= e(site_name) %>. Any edits you make will be stored against your public IP address (<code><%= e(ip) %></code>), which will be visible to anyone viewing the edit history of this page. If you wish to keep your IP address private, please log in."
:'page/edit/errors/no_content': "No content was provided for the page. Please try your edit again."

# Page - history - revisions list
:'page/history/revisions/diffs': "Diff"
:'page/history/revisions/timestamp': "Revision time"
:'page/history/revisions/author': "Author"
:'page/history/revisions/description': "Revision description"
:'page/distory/revisions/description/empty': "(no revision description provided)"
:'page/history/view_rev/notice': "You are viewing revision <code><%= e(rev) %></code>, created at <code><%= t(:'timestamp/fmt_ts', ts: ts) %></code>."
:'page/history/view_rev/notice/back_to_list': "Back to revision list"
:'page/history/view_rev/notice/toggle_raw': "Show page source: click to <% if display_raw %>disable<% else %>enable<% end %>"
:'page/history/view_diff/notice': "You are viewing the difference between revision <a href=\"<%= e(rev_one_link) %>\"><code><%= e(rev_one) %></code></a> and revision <a href=\"<%= e(rev_two_link) %>\"><code><%= e(rev_two) %></code></a>. Click on either of the revision IDs to show just that revision."
:'page/history/view_diff/notice/back_to_list': "Back to revision list"
:'page/history/view_diff/notice/reverse_revs': "Swap revisions"

# Errors
:'errors/not_found/title': "Page not found"
:'errors/not_found/content': "The page you have requested could not be found."
:'errors/not_found/not_logged_in': "You may be seeing this error because you're not logged in. Click the button below to log in."
:'errors/maintenance/title': "Maintenance"
:'errors/maintenance/content': "Sorry, <%= site_name %> is down for maintenance. Please check back later."
:'errors/maintenance/admin_login_link': "Site administrators can log in by clicking here."
:'errors/internal_server_error/title': "Internal server error"
:'errors/internal_server_error/content': "Something went wrong while processing your request. Please contact the site administrator and tell them about this, including what you were trying to do that lead you to this error."
:'errors/forbidden/title': "Access denied"
:'errors/forbidden/content': "You don't have permission to access this page."
:'errors/forbidden/not_logged_in': "You may be seeing this error because you're not logged in. Click the button below to log in."

# Miscellaneous things
:'powered_by_kakapo': "<%= site_name %> is powered by <a href=\"https://gitlab.com/phormium/kakapo\">Kākāpo</a>, a <a href=\"https://phormium.nz\">Phormium</a> project."
