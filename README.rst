Kākāpō
======

Kākāpō is a simple Wiki software, named after `the flightless parrot`_.

Kākāpō is not a "standalone" Wiki software - it requires an authentication
server implementing the Magenta_ single-sign-on protocol for logins. This
could be the standalone Magenta server, or another application that
implements the Magenta protocol, like EARMMS_ (in fact, Kākāpō was designed
to run alongside an EARMMS installation).

Support for OpenID Connect as an authentication method is on the roadmap,
and will ideally be implemented by the time Kākāpō v1.0 comes around.

.. _the flightless parrot: https://en.wikipedia.org/wiki/Kakapo
.. _Magenta: https://sr.ht/~ren/magenta
.. _EARMMS: https://gitlab.com/phormium/earmms

Installation
------------

Requirements:

+ Ruby, version 2.6 or above
+ Bundler, version 2.2 or above
+ Python, version 3.5 or above
+ The Python `Docutils`_ module, version 0.16 or above

.. _Docutils: https://docutils.sourceforge.io

To get started, clone the repository and install the dependencies::

    $ git clone --recursive https://gitlab.com/phormium/kakapo.git
    $ cd kakapo
    $ bundle install

Set up your environment variables, and then run the database migrations::

    $ bundle exec rake db:migrate
    $ bundle exec rake cfg:defaults

You should now be able to start the Kākāpō server::

    $ bundle exec puma

By default, the development server will set up self-signed TLS certificates,
and listen on port ``9292``.

License
-------

Copyright (C) 2021 Kākāpō contributors

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

The full text of the GNU Affero General Public License is available
in the ``LICENSE.md`` file in the root of the Kākāpō repository,
and can also be found at https://www.gnu.org/licenses/.
