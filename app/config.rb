module Kakapo::Config
  Dir.glob(File.join(Kakapo.root, 'app', 'config', '*.rb')).each do |f|
    require f
  end

  module_function
  def parsers
    list = Kakapo::Config.constants.map do |cname|
      p = Kakapo::Config.const_get(cname)
      next unless p.respond_to?(:accept?)

      p
    end

    list.compact!
    list.sort!{|a, b| a.order <=> b.order}
    list
  end
end
