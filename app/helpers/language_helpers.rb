require 'erb'

module Kakapo::Helpers::LanguageHelpers
  # This class exists to let translated text call t()
  class LanguageData < OpenStruct
    include Kakapo::Helpers::LanguageHelpers

    def e(text)
      ERB::Util.html_escape(text)
    end

    def site_name
      Kakapo.app_config['site-name']
    end

    def org_name
      Kakapo.app_config['org-name']
    end

    def base_url
      Kakapo.app_config['base-url']
    end

    def app_config
      Kakapo.app_config
    end
  end

  def languages
    ReConnect.languages
  end

  def current_language
    if defined?(session) && session.key?(:lang)
      return session[:lang]
    end

    Kakapo.default_language
  end

  def t(translation_key, values = {})
    language = current_language
    language = values.delete(:force_language) if values[:force_language]
    return translation_key.to_s if language == "translationkeys"

    text = Kakapo.languages[language]&.fetch(translation_key, nil)
    text = Kakapo.languages[Kakapo.default_language].fetch(translation_key, nil) unless text

    unless text
      return "##MISSING(#{translation_key.to_s})##"
    end

    values = LanguageData.new(values)
    template = Tilt::ERBTemplate.new() { text }
    template.render(values)
  end
end
