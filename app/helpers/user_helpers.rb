module Kakapo::Helpers::UserHelpers
  def current_token
    return nil unless session.key?(:token)

    t = Kakapo::Models::Token.where(token: session[:token], use: 'session').first
    return nil unless t
    return nil unless t.check_validity!

    t
  end

  def logged_in?
    return false if current_token&.user.nil?
    current_token.user.sso_method != 'ip_address'
  end

  def current_user
    current_token&.user
  end

  def create_ip_based_user
    user = Kakapo::Models::User.find_or_create(
      sso_method: 'ip_address',
      sso_external_id: request.ip
    ) do |u|
      u.username = request.ip
      u.email = nil
    end.save

    session[:token] = user.login!().token
    user
  end
end
