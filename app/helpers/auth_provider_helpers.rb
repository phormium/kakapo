require 'ostruct'
require 'magentasso'
require 'addressable'

module Kakapo::Helpers::AuthProviderHelpers
  class MagentaProvider < OpenStruct
    def begin_url
      "/auth/magenta/#{name}"
    end

    def request
      callback_url = ::Addressable::URI.parse(Kakapo.app_config['base-url'])
      callback_url += "/auth/magenta/#{name}/callback"

      MagentaSSO::Request.new(
        client_id,
        client_secret,
        nil,
        scopes,
        callback_url.to_s,
      )
    end

    def verify_response(payload, signature)
      MagentaSSO::Response.verify(payload, signature, client_secret)
    end
  end

  def auth_providers(opts = {})
    providers = {}

    Kakapo.app_config['magenta-providers'].each do |mp|
      data = {}
      %w[name friendly_name base_url client_id client_secret scopes].each do |key|
        data[key.to_sym] = mp[key] 
      end

      provider = MagentaProvider.new(data)
      providers[provider.name] = provider
    end

    providers
  end
end
