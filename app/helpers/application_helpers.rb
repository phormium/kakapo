module Kakapo::Helpers::ApplicationHelpers
  require_relative './language_helpers'
  include Kakapo::Helpers::LanguageHelpers

  require_relative './csrf_helpers'
  include Kakapo::Helpers::CsrfHelpers

  require_relative './theme_helpers'
  include Kakapo::Helpers::ThemeHelpers

  require_relative './flash_helpers'
  include Kakapo::Helpers::FlashHelpers

  require_relative './user_helpers'
  include Kakapo::Helpers::UserHelpers

  def site_name
    Kakapo.app_config["site-name"]
  end

  def org_name
    Kakapo.app_config["org-name"]
  end

  def current_prefix?(path = '/')
    request.path.start_with?(path) ? 'current' : nil
  end

  def current?(path = '/')
    request.path == path ? 'current' : nil
  end
end
