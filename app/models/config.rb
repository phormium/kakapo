class Kakapo::Models::Config < Sequel::Model(:config)
  def self.by_key(key)
    where(key: key.to_s).first
  end
end
