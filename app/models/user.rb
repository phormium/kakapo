require 'addressable'

class Kakapo::Models::User < Sequel::Model
  one_to_many :tokens

  def login!
    token = Kakapo::Models::Token.generate_long
    token.user = self
    token.use = "session"
    token.save

    token
  end

  def invalidate_tokens!
    invalidate_tokens_except!(nil)
  end

  def invalidate_tokens_except!(token)
    to_invalidate = Kakapo::Models::Token.where(
      user_id: self.id,
      use: 'session',
      valid: true,
    ).all

    unless token.nil?
      token = token.token if token.respond_to?(:token)
      to_invalidate.reject!{|x| x.token == token}
    end

    to_invalidate.map(&:invalidate!)
  end

  def delete!
    self.tokens.map(&:delete)
    self.delete
  end
end
