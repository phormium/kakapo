require 'base32'
require 'securerandom'

class Kakapo::Models::Token < Sequel::Model
  TOKEN_RANDOM_LEN = 64
  TOKEN_LENGTH_LONG = 16
  TOKEN_LENGTH_SHORT = 40

  many_to_one :user

  def self.generate_long
    token = Base32.encode(SecureRandom.random_bytes(TOKEN_RANDOM_LEN))
    token = token[0..(TOKEN_LENGTH_LONG - 1)].downcase
    self.new(token: token, valid: true, expiry: nil)
  end

  def self.generate_short
    token = Base32.encode(SecureRandom.random_bytes(TOKEN_RANDOM_LEN))
    token = token[0..(TOKEN_LENGTH_SHORT - 1)].downcase
    self.new(token: token, valid: true, expiry: nil)
  end

  def check_validity!
    return false unless self.valid

    if self.expiry && Time.now >= self.expiry
      self.invalidate! if self.valid
      return false
    end

    return true
  end

  def invalidate!
    self.valid = false
    self.save
  end
end
