require 'rbst'

class Kakapo::Models::PageRevision < Sequel::Model
  many_to_one :page

  def self.to_html(content, *args)
    args.unshift(:strip_comments)
    args.unshift(:no_file_insertion)
    args.unshift(:cloak_email_addresses)
    args << {} unless args.last.is_a?(Hash)
    args.last.merge!({
      input_encoding_error_handler: :replace,
      output_encoding_error_handler: :replace,
    })

    RbST.new(content).to_html(*args)
  end

  def to_html(*args)
    self.class.to_html(self.content, *args)
  end

  def title
    t = to_html(part: :title)&.strip
    return nil if t.nil? || t&.empty?
    t
  end

  def subtitle
    t = to_html(part: :subtitle)&.strip
    return nil if t.nil? || t&.empty?
    t
  end
end
