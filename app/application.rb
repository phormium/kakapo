class Kakapo::Application < Sinatra::Base
  helpers Kakapo::Helpers::ApplicationHelpers

  set :environment, ENV["RACK_ENV"] ||= "production"
  set :default_encoding, "UTF-8"
  set :views, File.join('app', 'views')
  set :haml, format: :html5, default_encoding: "UTF-8"
  enable :sessions

  not_found do
    ctrl = Kakapo::Controllers::ErrorController.new(self)
    ctrl.preflight
    ctrl.before if ctrl.respond_to?(:before)

    ctrl.not_found
  end

  error 403 do
    ctrl = Kakapo::Controllers::ErrorController.new(self)
    ctrl.preflight
    ctrl.before if ctrl.respond_to?(:before)

    ctrl.forbidden
  end

  error do
    ctrl = Kakapo::Controllers::ErrorController.new(self)
    ctrl.preflight
    ctrl.before if ctrl.respond_to?(:before)

    ctrl.server_error
  end
end
