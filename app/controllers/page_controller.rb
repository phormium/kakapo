class Kakapo::Controllers::PageController < Kakapo::Controllers::ApplicationController
  def before
    unless Kakapo.app_config['anonymous-view']
      return halt 403 unless logged_in?
    end
  end
end
