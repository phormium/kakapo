require 'differ'
                                                                                
class Kakapo::Controllers::PageRevisionController < Kakapo::Controllers::PageController
  add_route :get, "/:revision"
  add_route :get, "/diff/:rev_one/:rev_two", method: :diff

  def before
    super

    unless Kakapo.app_config['anonymous-history']
      return halt 403 unless logged_in?
    end
  end

  def index(revision)
    @revision = Kakapo::Models::PageRevision[revision]
    return halt 404 unless @revision
    @page = Kakapo::Models::Page[@revision.page_id]
    return halt 404 unless @page

    @title = @revision.title || Kakapo::Utils.slug_to_title(@page.slug)
    @subtitle = @revision.subtitle
    @content = @revision.to_html(part: :fragment)

    @display_raw = request.params['raw'].to_i.positive?

    return haml(:'page/history/view_rev', layout: :layout, locals: {
      slug: @page.slug,
      page: @page,
      revision: @revision,
      title: @title, 
      subtitle: @subtitle,
      content: @content,
      content_raw: @revision.content,
      display_raw: @display_raw,
    })
  end

  def diff(rev_one, rev_two)
    @rev_one = Kakapo::Models::PageRevision[rev_one]
    return halt 404 unless @rev_one
    @rev_two = Kakapo::Models::PageRevision[rev_two]
    return halt 404 unless @rev_two
    return halt 404 unless @rev_one.page_id == @rev_two.page_id
    @page = Kakapo::Models::Page[@rev_one.page_id]
    return halt 404 unless @page

    @title = @rev_one.title || Kakapo::Utils.slug_to_title(@page.slug)
    @subtitle = @rev_one.subtitle

    @diff = Differ.diff_by_word(@rev_one.content, @rev_two.content)
    @diff_render = @diff.format_as(:html)

    return haml(:'page/history/view_diff', layout: :layout, locals: {
      slug: @page.slug,
      page: @page,
      rev_one: @rev_one,
      rev_two: @rev_two,
      title: @title, 
      subtitle: @subtitle,
      diff: @diff,
      diff_render: @diff_render,
    })
  end
end
