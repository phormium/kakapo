class Kakapo::Controllers::PageEditController < Kakapo::Controllers::PageController
  add_route :get, "/:slug"
  add_route :post, "/:slug"

  def before
    super

    unless Kakapo.app_config['anonymous-edit']
      return halt 403 unless logged_in?
    end
  end

  def index(slug)
    slug.downcase!

    @page = Kakapo::Models::Page.by_slug(slug)
    @title = @page&.title || Kakapo::Utils.slug_to_title(slug)
    @content = @page&.current_revision&.content

    if request.post?
      content = request.params['content']&.strip
      content = nil if content&.empty?
      if content.nil?
        flash :error, t(:'page/edit/errors/no_content')
        return redirect request.path
      end

      @revdesc = request.params['revdesc']&.strip
      @revdesc = nil if @revdesc&.empty?

      # a return to the editor from the preview page
      if request.params['editor'].to_i.positive?
        return haml(:'page/edit/index', layout: :layout, locals: {
          slug: slug,
          page: @page,
          title: @title,
          content: content,
          revdesc: @revdesc,
        })
      end

      # actual save operation
      if request.params['save'].to_i.positive?
        # if not logged in, create an IP based user
        unless logged_in?
          create_ip_based_user()
        end

        # create a new revision
        @revision = Kakapo::Models::PageRevision.new(
          content: content,
          creator: current_user.id,
          description: @revdesc,
        ).save

        # is this a new page?
        unless @page
          @page = Kakapo::Models::Page.new(
            slug: slug,
            current_revision_id: @revision.id,
          )
        end

        @page.update(current_revision_id: @revision.id).save
        @revision.update(page_id: @page.id).save

        flash :success, t(:'page/edit/success')
        return redirect url("/w/#{slug}")
      end

      title = Kakapo::Models::PageRevision.to_html(content, part: :title)&.strip
      title = nil if title&.empty?
      title = Kakapo::Utils.slug_to_title(slug) if title.nil?

      subtitle = Kakapo::Models::PageRevision.to_html(content, part: :subtitle)&.strip
      subtitle = nil if subtitle&.empty?
      
      fragment = Kakapo::Models::PageRevision.to_html(content, part: :fragment)&.strip
      fragment = nil if fragment&.empty?

      return haml(:'page/edit/preview', layout: :layout, locals: {
        slug: slug,
        page: @page,
        title: title,
        subtitle: subtitle,
        content: fragment,
        raw_content: content,
        revdesc: @revdesc,
      })
    end

    return haml(:'page/edit/index', layout: :layout, locals: {
      slug: slug,
      page: @page, 
      title: @title,
      content: @content,
      revdesc: nil,
    })
  end
end
