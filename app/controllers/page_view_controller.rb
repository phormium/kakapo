class Kakapo::Controllers::PageViewController < Kakapo::Controllers::PageController
  add_route :get, "/:slug"

  def index(slug)
    slug.downcase!

    @page = Kakapo::Models::Page.by_slug(slug)
    @title = @page&.title || Kakapo::Utils.slug_to_title(slug)
    @subtitle = @page&.subtitle
    @content = @page&.to_html(part: :fragment) || haml(
      :'page/partials/not_found',
      layout: false,
      locals: {
        slug: slug,
        title: @title,
      },
    )

    return haml(:'page/view', layout: :layout, locals: {
      slug: slug,
      page: @page,
      title: @title, 
      subtitle: @subtitle,
      content: @content,
    })
  end
end
