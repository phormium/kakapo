class Kakapo::Controllers::ApplicationController
  extend Kakapo::Route

  def initialize(app)
    @app = app
  end

  def method_missing(meth, *args, &bk)
    @app.instance_eval do
      self.send(meth, *args, &bk)
    end
  end

  def preflight
    # Set and check CSRF
    csrf_set!
    unless request.safe? 
      unless csrf_ok?
        return halt 403, "CSRF failed"
      end
    end

    @app.instance_eval do
      # get logged in user
      @current_user = current_token&.user

      # Render sidebar
      if logged_in? || Kakapo.app_config['anonymous-view']
        @sidebar_content = Kakapo::Models::Page
          .by_slug('special:sidebar')
          &.to_html(:strip_comments, part: :fragment)
          &.strip
      end
    end
  end
end
