require 'addressable'

class Kakapo::Controllers::AuthMagentaController < Kakapo::Controllers::ApplicationController
  include Kakapo::Helpers::AuthProviderHelpers

  add_route :get, "/:clientid"
  add_route :get, "/:clientid/callback", method: :callback

  def before
    return redirect url("/") if logged_in?
    @providers = auth_providers()
  end

  def index(clientid)
    @provider = @providers[clientid]
    return halt 404 unless @provider

    @request = @provider.request
    session[:magenta_nonce] = @request.nonce

    redir_url = ::Addressable::URI.parse(@provider.base_url)
    redir_url.query_values = @request.query_params

    return redirect redir_url.to_s
  end

  def callback(clientid)
    @provider = @providers[clientid]
    return halt 404 unless @provider

    payload = request.params['payload']&.strip
    payload = nil if payload&.empty?
    signature = request.params['signature']&.strip
    signature = nil if signature&.empty?
    if payload.nil? || signature.nil?
      return haml(:'auth/generic_error', locals: {
        title: t(:'auth/generic_error/title'),
      })
    end

    @response = @provider.verify_response(payload, signature)
    unless @response && @response.nonce == session[:magenta_nonce]
      return haml(:'auth/generic_error', locals: {
        title: t(:'auth/generic_error/title'),
      })
    end

    @user = Kakapo::Models::User.find_or_create(
      sso_method: "magenta:#{@provider.name}",
      sso_external_id: @response.external_id.to_s,
    ) do |u|
      u.username = Kakapo::Models::Token.generate_short.token[0..11].downcase

      flash :success, t(:'auth/new_user_welcome')
    end

    @user.email = @response.email_address
    @user.save

    session[:token] = @user.login!().token
    flash :success, t(:'auth/success', provider: @provider.friendly_name, username: @user.username)

    next_url = session.delete(:after_login)&.strip
    next_url = url("/") if next_url.nil? || next_url&.empty?
    return redirect next_url
  end
end
