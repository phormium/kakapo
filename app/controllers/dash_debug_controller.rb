class Kakapo::Controllers::DashDebugController < Kakapo::Controllers::ApplicationController
  add_route :get, "/restart", method: :restart
  add_route :get, "/lang-reload", method: :lang_reload

  def before
    return halt 404 unless settings.development?
  end

  def restart
    Kakapo::ServerUtils.app_server_restart!

    flash :success, "Issued server restart request."
    redirect back
  end

  def lang_reload
    Kakapo.load_languages

    flash :success, "Reloaded languages."
    redirect back
  end
end
