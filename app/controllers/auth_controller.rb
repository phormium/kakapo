class Kakapo::Controllers::AuthController < Kakapo::Controllers::ApplicationController
  include Kakapo::Helpers::AuthProviderHelpers

  add_route :get, "/"
  add_route :get, "/logout", method: :logout

  def before
    @providers = auth_providers()
  end

  def index
    @next = request.params['next']&.strip
    @next = url("/") if @next.nil? || @next&.empty?
    return redirect url(@next) if logged_in?
    session[:after_login] = @next

    if @providers.empty?
      return haml(:'auth/generic_error', locals: {
        title: t(:'auth/generic_error/title'),
      })
    end

    if @providers.count == 1
      return redirect url(@providers.first.last.begin_url)
    end

    haml(:'auth/choose_provider', locals: {
      title: t(:'auth/choose_provider/title'),
      providers: @providers,
    })
  end

  def logout
    current_token.invalidate!
    session.delete(:token)

    flash :success, t(:'auth/logout/success')
    return redirect url("/")
  end
end
