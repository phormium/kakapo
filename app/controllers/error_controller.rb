class Kakapo::Controllers::ErrorController < Kakapo::Controllers::ApplicationController
  add_route :get, "/403", method: :forbidden
  add_route :get, "/404", method: :not_found
  add_route :get, "/500", method: :server_error

  def forbidden
    haml(:'errors/forbidden', locals: {
      title: t(:'errors/forbidden/title'),
    })
  end

  def not_found
    haml(:'errors/not_found', locals: {
      title: t(:'errors/not_found/title'),
    })
  end

  def server_error
    haml(:'errors/internal_server_error', layout: :layout_minimal, locals: {
      title: t(:'errors/internal_server_error/title'),
      no_flash: true,
    })
  end
end
