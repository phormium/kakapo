class Kakapo::Controllers::UserSettingsController < Kakapo::Controllers::ApplicationController
  add_route :get, "/"
  add_route :post, "/username", method: :username

  def before
    unless logged_in?
      return redirect url("/auth?next=/u:settings") 
    end

    @current_user = current_user
    @username = @current_user.username
  end

  def index
    haml(:'user/settings', locals: {
      title: t(:'user/settings/title'),
      user: @current_user,
      username: @username,
    })
  end

  def username
    @new_username = request.params['username']&.strip&.downcase
    if @new_username == @username
      return redirect back
    end

    if @new_username != @new_username.gsub(/[^a-z0-9]/, '')
      flash :error, t(:'user/settings/username/errors/special_characters')
      return redirect back
    end

    # check if anyone else has this username
    if Kakapo::Models::User.where(username: @new_username).count.positive?
      flash :error, t(:'user/settings/username/errors/already_exists')
      return redirect back
    end

    @current_user.username = @new_username
    @current_user.save

    flash :success, t(:'user/settings/username/success', username: @new_username)
    return redirect back
  end
end
