class Kakapo::Controllers::IndexController < Kakapo::Controllers::ApplicationController
  add_route :get, "/"

  def index
    return redirect url("/w/special:home")
  end
end
