class Kakapo::Controllers::PageHistoryController < Kakapo::Controllers::PageController
  add_route :get, "/:slug"

  def before
    super

    unless Kakapo.app_config['anonymous-history']
      return halt 403 unless logged_in?
    end
  end

  def index(slug)
    slug.downcase!

    @page = Kakapo::Models::Page.by_slug(slug)
    return halt 404 unless @page

    @title = @page&.title || Kakapo::Utils.slug_to_title(slug)
    @revisions = @page.page_revisions.map do |rev|
      creator = Kakapo::Models::User[rev.creator]

      {
        id: rev.id,
        description: rev.description,
        created: rev.created,
        creator: {
          id: creator.id,
          username: creator.username,
        },
      }
    end.sort { |a, b| b[:created] <=> a[:created] }

    return haml(:'page/history/index', layout: :layout, locals: {
      slug: slug,
      page: @page,
      title: @title, 
      revisions: @revisions,
    })
  end
end
