module Kakapo::Helpers
  # Load ApplicationHelpers first
  require File.join(Kakapo.root, 'app', 'helpers', 'application_helpers')

  # And then load the rest
  Dir.glob(File.join(Kakapo.root, 'app', 'helpers', '*.rb')).each do |f|
    require f
  end
end
