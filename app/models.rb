module Kakapo::Models
  def self.load_models
    Dir.glob(File.join(Kakapo.root, 'app', 'models', '*.rb')).each do |f|
      require f
    end
  end
end
